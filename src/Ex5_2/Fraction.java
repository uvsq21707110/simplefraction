package Ex5_2;

public final class Fraction {

    private int num;

    private int den;

    public static final Fraction ZERO = new Fraction(0,1);

    public static final Fraction UN = new Fraction(1,1);

    public Fraction(int num, int den){
        this.num = num;
        this.den = den;
    }

    public Fraction(int num){
        this.num = num;
        this.den = 1;
    }

    public Fraction(){
        this.num = 0;
        this.den = 1;
    }

    public int getNum() {
        int ret = this.num;
        return ret;
    }

    public int getDen() {
        int ret = this.den;
        return ret;
    }

    public double doubleValue(){
        double d = (double)num/(double)den;
        return d;
    }

    private int pgcd(int a, int b){
        int r;
        while (b != 0)
        {
            r = a%b;
            a = b;
            b = r;
        }
        return a;
    }

    public Fraction add(Fraction a){

        int nNum = num*a.den + den*a.num;
        int nDen = den*a.den;

        int pgcd = pgcd(nNum,nDen);
        return new Fraction(nNum/pgcd,nDen/pgcd);
    }

    @Override
    public boolean equals(Object object){
        Fraction fraction = (Fraction) object;
        int pgcdF = pgcd(fraction.num, fraction.den);
        int pgcdThis = pgcd(this.num, this.den);
        if(fraction.num/pgcdF == this.num/pgcdThis && fraction.den/pgcdF == this.den/pgcdThis)
            return true;
        return false;
    }

    @Override
    public String toString(){
        if(this.den != 1)
            return this.num + "/" + this.den;
        return this.num + "";
    }

    public int compareTo(Fraction fraction){
        if(fraction.doubleValue()<this.doubleValue())
            return 1;
        else if(fraction.doubleValue()>this.doubleValue())
            return -1;
        return 0;
    }
}
