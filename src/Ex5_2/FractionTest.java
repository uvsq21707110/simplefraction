package Ex5_2;

import org.junit.Test;

import static org.junit.Assert.*;

public class FractionTest {

    @Test
    public void FractionTest(){
        Fraction f = new Fraction();
        assertNotNull(f);
        f = new Fraction(1);
        assertNotNull(f);
        f = new Fraction(1,1);
        assertNotNull(f);
    }

    @Test
    public void getNumTest(){
        Fraction f = new Fraction();
        assertEquals(f.getNum(),0);
    }

    @Test
    public void getDenTest(){
        Fraction f = new Fraction();
        assertEquals(f.getDen(),1);
    }

    @Test
    public void doubleValueTest(){
        Fraction f = new Fraction(2,3);
        double d = (double)2/(double)3;
        assertEquals(f.doubleValue(),d,0.0001);
    }

    @Test
    public void addTest(){
        Fraction f1 = new Fraction(1,6);
        Fraction f2 = new Fraction(5,6);
        Fraction add = f1.add(f2);
        assertEquals(add.doubleValue(),1d, 0.0001);
    }

    @Test
    public void equalsTest(){
        Fraction f1 = new Fraction();
        Fraction f2 = new Fraction();
        Fraction f3 = new Fraction(1);
        assertTrue(f1.equals(f2));
        assertFalse(f2.equals(f3));
    }

    @Test
    public void compareToTest(){
        Fraction f1 = new Fraction(1,3);
        Fraction f2 = new Fraction(2,3);
        Fraction f3 = new Fraction(3,9);
        assertEquals(f1.compareTo(f2),-1);
        assertEquals(f1.compareTo(f3),0);
        assertEquals(f2.compareTo(f1),1);
    }

    @Test
    public void toStringTest(){
        Fraction f1 = new Fraction(2,3);
        Fraction f2 = new Fraction();
        assertEquals(f1.toString(),"2/3");
        assertEquals(f2.toString(),"0");
    }

}
