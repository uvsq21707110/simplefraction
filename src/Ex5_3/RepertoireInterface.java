package Ex5_3;

import java.util.ArrayList;

/**
 * Created by ChLesPaul on 18/10/2017.
 */
public interface RepertoireInterface {

    int getTaille();

    String getNom();

    RepertoireInterface getPere();

    void setPere(RepertoireInterface newPere);

    ArrayList<RepertoireInterface> getRepertoires();

    boolean luiMeme(RepertoireInterface repertoireInterface);



}
