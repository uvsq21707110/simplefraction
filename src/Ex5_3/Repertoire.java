package Ex5_3;

import java.util.ArrayList;

/**
 * Created by ChLesPaul on 18/10/2017.
 */
public class Repertoire implements RepertoireInterface {

    private String nom;
    private ArrayList<RepertoireInterface> repertoires;
    private RepertoireInterface pere;

    public Repertoire(String nom){
        this.nom = nom;
        this.repertoires = new ArrayList<>();
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public RepertoireInterface getPere(){
        return this.pere;
    }

    @Override
    public void setPere(RepertoireInterface pere){
        this.pere = pere;
    }

    @Override
    public ArrayList<RepertoireInterface> getRepertoires() {
        return this.repertoires;
    }

    public boolean ajouter(RepertoireInterface repertoire){
        if(!this.luiMeme(repertoire)){
            this.repertoires.add(repertoire);
            if(repertoire.getPere()!=null){
                Repertoire p = (Repertoire)repertoire.getPere();
                p.unajouter(repertoire);
            }
            repertoire.setPere(this);
            return true;
        }
        return false;
    }

    @Override
    public boolean luiMeme(RepertoireInterface newPere){
        boolean ans = false;
        if(this.nom.equals(newPere.getNom()))
            return true;
        for(RepertoireInterface actual: newPere.getRepertoires()){
            ans |= luiMeme(actual);
        }
        return ans;
    }

    private void unajouter(RepertoireInterface repertoire){
        this.getRepertoires().remove(repertoire);
    }

    @Override
    public int getTaille(){
        int taille = 0;
        for (RepertoireInterface repertoire: this.repertoires) {
            taille += repertoire.getTaille();
        }
        return taille;
    }
}
