package Ex5_3;

import java.util.ArrayList;

/**
 * Created by ChLesPaul on 18/10/2017.
 */
public class Main5_3 {

    public static void main(String[] args){
        Fichier fa = new Fichier("fa", 1);
        Fichier fb = new Fichier("fb", 2);
        Fichier fc = new Fichier("fc", 3);
        Repertoire a = new Repertoire("a");
        assert (!a.ajouter(a));
        assert (a.ajouter(fb));
        Repertoire b = new Repertoire("b");
        assert (b.ajouter(fb));
        assert (a.ajouter(b));
        assert (!b.ajouter(a));
        Repertoire c = new Repertoire("c");
        assert (a.ajouter(c));
        assert (!c.ajouter(a));
        assert (a.ajouter(fa));
        assert (c.ajouter(fc));
        assert (a.getTaille()==6);
        assert (b.getTaille()==2);
        assert (c.getTaille()==3);
    }
}
