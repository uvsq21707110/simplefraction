package Ex5_3;

import java.util.ArrayList;

/**
 * Created by ChLesPaul on 18/10/2017.
 */
public class Fichier implements RepertoireInterface{

    private String nom;
    private int taille;
    private RepertoireInterface pere;

    public Fichier(String nom, int taille){
        this.nom = nom;
        this.taille = taille;
    }

    @Override
    public int getTaille(){
        return taille;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public RepertoireInterface getPere() {
        return this.pere;
    }

    @Override
    public void setPere(RepertoireInterface newPere) {
        this.pere = newPere;
    }

    @Override
    public ArrayList<RepertoireInterface> getRepertoires() {
        return new ArrayList<>();
    }

    @Override
    public boolean luiMeme(RepertoireInterface repertoireInterface) {
        return true;
    }


}
