package Ex5_1;

/**
 * Created by ChLesPaul on 17/10/2017.
 */
public class Dvd extends Document{

    private int annee;

    private String realisateur;

    public Dvd(String titre, int annee, String realisateur){
        super(titre);
        this.annee = annee;
        this.realisateur = realisateur;
    }

    @Override
    public String getCreator() {
        return realisateur;
    }

    @Override
    public String getInformations() {
        return super.getInformations() + ", realisateur: " + realisateur + ", annee: " + annee;
    }
}
