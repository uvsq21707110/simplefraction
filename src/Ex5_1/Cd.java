package Ex5_1;

/**
 * Created by ChLesPaul on 17/10/2017.
 */
public class Cd extends Document {

    private int nombreTitres;

    private String artiste;

    public Cd(String nom, String artiste, int nombreTitres){

        super(nom);
        this.artiste = artiste;
        this.nombreTitres = nombreTitres;
    }

    @Override
    public String getCreator() {
        return artiste;
    }

    @Override
    public String getInformations() {
        return super.getInformations() + ", artiste: " + artiste + ", nombre: " + nombreTitres;
    }
}
