package Ex5_1;

/**
 * Created by ChLesPaul on 17/10/2017.
 */
public class Document {

    private String nom;

    public Document(String nom){
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public String getCreator() {
        return "";
    }

    public String getInformations(){
        return "Nom: " + nom;
    }
}
