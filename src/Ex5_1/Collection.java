package Ex5_1;

import java.util.ArrayList;

/**
 * Created by ChLesPaul on 17/10/2017.
 */
public class Collection {

    private ArrayList<Document> documents;

    public Collection(){
        this.documents = new ArrayList<>();
    }

    public ArrayList<Document> rechercher(String cle){

        ArrayList<Document> trouves = new ArrayList<>();

        for (Document document:documents) {
            if(document.getNom().contains(cle)||document.getCreator().contains(cle))
                trouves.add(document);
        }

        return trouves;
    }

    public void ajouter(Document nouveau){
        documents.add(nouveau);
    }

    public ArrayList<String> lister(){
        ArrayList<String> informations = new ArrayList<>();
        for (Document document : documents) {
            informations.add(document.getInformations());
        }
        return informations;
    }

}
