package Ex5_1;

import java.util.ArrayList;

/**
 * Created by ChLesPaul on 18/10/2017.
 */
public class Main5_1 {

    public static void main(String[] args){
        Collection collection = new Collection();

        Cd cd1 = new Cd("cd1", "artiste1", 1);
        Cd cd2 = new Cd("cd2", "artiste2", 2);

        Dvd dvd1 = new Dvd("dvd1", 1, "realisateur1");
        Dvd dvd2 = new Dvd("dvd2", 2, "realisateur2");

        collection.ajouter(cd1);
        collection.ajouter(dvd1);
        collection.ajouter(cd2);
        collection.ajouter(dvd2);

        ArrayList<Document> trouves = collection.rechercher("cd");
        for (Document document: trouves) {
            System.out.println(document.getInformations());
        }


    }
}
