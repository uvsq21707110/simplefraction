package Ex4_2;

public class Main {

    public static void main (String[] args){
        Serveur s = new Serveur();
        Client c1 = new Client("c1");
        Client c2 = new Client("c2");
        c1.seConnecter(s);
        c2.seConnecter(s);
        Client c3 = new Client("c3");
        c3.seConnecter(s);
        c3.envoyer("Bonjour");
    }

}
