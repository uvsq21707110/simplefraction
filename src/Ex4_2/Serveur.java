package Ex4_2;

import java.util.ArrayList;

public class Serveur {

    private ArrayList<Client> clients;

    public Serveur(){
        this.clients = new ArrayList<>();
    }

    public boolean connecter(Client client){

        for (Client actual:this.clients) {
            if(actual.nom.equals(client.nom))
                return false;
        }
        this.clients.add(client);
        return true;
    }

    public void diffuser(String message){
        for (Client client:this.clients) {
            System.out.println(client.recevoir(message));
        }
    }
}
