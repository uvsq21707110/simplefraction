package Ex4_2;

public class Client {

    public String nom;

    private Serveur serveur;

    public Client(String nom){
        this.nom = nom;
    }

    public boolean seConnecter(Serveur serveur){
        if(serveur!=null){
            boolean connect = serveur.connecter(this);
            if(connect)
                this.serveur = serveur;
        }
        return false;
    }

    public void envoyer(String message){
        if(serveur!=null)
            serveur.diffuser(message);
    }

    public String recevoir(String message){
        return this.nom + " reçu: " + message;
    }
}
