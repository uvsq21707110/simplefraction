package Ex4_1;

/**
 * Cette class permet de chiffrer/déchiffrer une chaîne de caractères (composée de lettres majuscules et d’espace)
 * Si la chaîne est null alors il sera traité comme une chaîne vide
 */
public class ChaineCryptee {

    private String cryptee;

    private int decalage;

    private ChaineCryptee (String enClair, int decalage){
        this.decalage = decalage;
        this.cryptee= (enClair!=null) ? crypte(enClair):new String();

    }

    public ChaineCryptee deCryptee(String chaine, int decalage){
        return new ChaineCryptee(decrypte(chaine, decalage), decalage);
    }

    public ChaineCryptee deEnClair(String chaine, int decalage){
        return new ChaineCryptee(chaine, decalage);
    }

    private static String decrypte(String cryptee, int decalage) {
        String enClair = new String();
        char[] array = cryptee.toCharArray();
        for(int i = 0; i < array.length; i++){
            enClair += caleCaractere(array[i],decalage);
        }
        return enClair;
    }

    public String decrypte() {
        String enClair = new String();
        char[] array = cryptee.toCharArray();
        for(int i = 0; i < array.length; i++){
            enClair += caleCaractere(array[i],this.decalage);
        }
        return enClair;
    }

    public String crypte(){
        return this.cryptee;
    }

    private String crypte(String enClair){
        String cryptee = new String();
        char[] array = enClair.toCharArray();
        for(int i = 0; i < array.length; i++){
            cryptee += decaleCaractere(array[i],this.decalage);
        }
        return cryptee;
    }

    private static char decaleCaractere(char c, int decalage){
        return (c < 'A' || c > 'Z')? c:(char)(((c - 'A' + decalage)%26)+'A');
    }

    private static char caleCaractere(char c, int decalage){
        return (c < 'A' || c > 'Z')? c:(char)(((c - 'A' - decalage)%26)+'A');
    }

}
