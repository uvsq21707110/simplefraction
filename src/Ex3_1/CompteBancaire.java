package Ex3_1;

/**
 * Created by user on 05/10/17.
 */
public class CompteBancaire {

    private float solde;


    public CompteBancaire (float solde){
        if(solde<0)
            throw new IllegalArgumentException("Solde negatif");
        this.solde = solde;
    }

    public float consultation() {
        return solde;
    }

    public boolean credit(float value){
        if(value>=0){
            this.solde += value;
            return true;
        }

        return false;
    }

    public boolean debit(float value){
        if(this.solde>=value && value>=0){
            this.solde -= value;
            return true;
        }

        return false;

    }

    public boolean virement(float value, CompteBancaire compte){

        if(debit(value))
            return compte.credit(value);

        return false;
    }


}
