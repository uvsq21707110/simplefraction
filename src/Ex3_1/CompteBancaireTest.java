package Ex3_1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by user on 05/10/17.
 */
public class CompteBancaireTest {

    @Test (expected = IllegalArgumentException.class)
    public void testCompteBancaireSuccess(){
        CompteBancaire compteBancaire = new CompteBancaire(-1f);
    }

    @Test
    public void testCompteBancaireFailure (){
        CompteBancaire compteBancaire = new CompteBancaire(1f);
        assertNotNull(compteBancaire);
    }

    @Test
    public void testConsultation(){
        CompteBancaire compteBancaire = new CompteBancaire(1f);
        assertEquals(compteBancaire.consultation(),1f,0.001f);
    }

    @Test
    public void testDebit(){
        CompteBancaire compteBancaire = new CompteBancaire(10f);
        assertTrue(!compteBancaire.debit(11f));
        assertEquals(compteBancaire.consultation(),10f,0.001f);
        assertTrue(!compteBancaire.debit(-1f));
        assertEquals(compteBancaire.consultation(),10f,0.001f);
        assertTrue(compteBancaire.debit(10f));
        assertEquals(compteBancaire.consultation(),0f,0.001f);
    }

    @Test
    public void testcredit(){
        CompteBancaire compteBancaire = new CompteBancaire(10f);
        assertTrue(!compteBancaire.credit(-1f));
        assertEquals(compteBancaire.consultation(),10f,0.001f);
        assertTrue(compteBancaire.credit(10f));
        assertEquals(compteBancaire.consultation(),20f,0.001f);
    }

    @Test
    public void testVirement(){
        CompteBancaire compteBancaireA = new CompteBancaire(10f);
        CompteBancaire compteBancaireB = new CompteBancaire(10f);
        assertTrue(!compteBancaireA.virement(-1f,compteBancaireB));
        assertEquals(compteBancaireA.consultation(),10f,0.001f);
        assertEquals(compteBancaireB.consultation(),10f,0.001f);
        assertTrue(!compteBancaireA.virement(11f,compteBancaireB));
        assertEquals(compteBancaireA.consultation(),10f,0.001f);
        assertEquals(compteBancaireB.consultation(),10f,0.001f);
        assertTrue(compteBancaireA.virement(10f,compteBancaireB));
        assertEquals(compteBancaireA.consultation(),00f,0.001f);
        assertEquals(compteBancaireB.consultation(),20f,0.001f);
    }
}
